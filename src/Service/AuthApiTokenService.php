<?php
/**
 * Created by PhpStorm.
 * User: ileda
 * Date: 2019-03-09
 * Time: 12:01 PM
 */

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManager;

class AuthApiTokenService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * AuthApiTokenService constructor.
     * @param $entityManager
     */
    public function __construct($entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $request
     * @return User|bool
     */
    public function checkAuthToken($request)
    {
        $userRepo = $this->em->getRepository(User::class);
        $api_token = $request->query->get('api_token');

        if (!isset($api_token)) {
            return false;
        }

        /**
         * @var User $user
         */
        $user = $userRepo->findOneBy(['apiToken' => $api_token]);

        return $user;
    }

    /**
     * @return \Tmdb\Client
     */
    public function getTmdbClient()
    {
        $token  = new \Tmdb\ApiToken('f200ea93d28d03201a0e1caee1ebd3e6');
        $client = new \Tmdb\Client($token);

        return $client;
    }
}