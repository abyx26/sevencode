<?php
/**
 * Created by PhpStorm.
 * User: ileda
 * Date: 2019-03-08
 * Time: 8:45 PM
 */

namespace App\Controller;

use App\Entity\FavoriteMovie;
use App\Entity\User;
use App\Service\AuthApiTokenService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Hoa\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tmdb\Exception\TmdbApiException;

class ApiController extends AbstractFOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * * @throws \Exception
     * GET Route annotation.
     * @Get("/api/v1/movies")
     */
    public function getMovies(Request $request)
    {
        $authService = new AuthApiTokenService($this->getDoctrine()->getManager());

        if (!$authService->checkAuthToken($request)) {
            return $this->json([
                'error' => "You are not authorized!"
            ], Response::HTTP_UNAUTHORIZED);
        }

        $client = $authService->getTmdbClient();
        $query = $request->query->get('query');

        if (!isset($query)) {
            return $this->json([
                'error' => "You need a query param!"
            ], Response::HTTP_BAD_REQUEST);
        }

        try{
            if (preg_match('/^[0-9]+$/', $query)){
                $movies = [
                    'movie' => $client->getMoviesApi()->getMovie($query),
                    'related_movies' => $client->getMoviesApi()->getRecommendations($query)
                ];
            } else {
                $movies = $client->getSearchApi()->searchMovies($query);

                if (!$movies['total_results']) {
                    return $this->json([
                        'error' => 'There is no movie that has title: ' . $query . ''
                    ], Response::HTTP_NOT_FOUND);
                }
            }
        } catch (TmdbApiException $e){
            return $this->json([
                'error' => $e->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json([
            'movies' => $movies
        ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * GET Route annotation.
     * @Get("/api/v1/favorites")
     */
    public function getFavoriteMovies(Request $request)
    {
        $authService = new AuthApiTokenService($this->getDoctrine()->getManager());
        $user = $authService->checkAuthToken($request);

        if (!$user) {
            return $this->json([
                'error' => "You are not authorized!"
            ], Response::HTTP_UNAUTHORIZED);
        }

        $favoriteMovies = $user->getFavoriteMovies()->getValues();

        $moviesId = array_map(function($e) {
            return $e->getMovieId();
        }, $favoriteMovies);

        $client = $authService->getTmdbClient();
        $movies = [];

        foreach ($moviesId as $id) {
            $movies[] = $client->getMoviesApi()->getMovie($id);
        }

        return $this->json([
            'movies' => $movies
        ], Response::HTTP_OK);
    }

    /**
     * @throws \Exception
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * PUT Route annotation.
     * @Put("/api/v1/favorites/{id<[0-9]+>}")
     */
    public function addFavoriteMovie($id, Request $request)
    {
        $authService = new AuthApiTokenService($this->getDoctrine()->getManager());
        $entityManager = $this->getDoctrine()->getManager();
        $favoriteMovieRepo = $entityManager->getRepository(FavoriteMovie::class);
        $user = $authService->checkAuthToken($request);
        $current_date  = new \DateTime();

        if (!$user) {
            return $this->json([
                'error' => "You are not authorized!"
            ], Response::HTTP_UNAUTHORIZED);
        }

        $client = $authService->getTmdbClient();
        try{
            $client->getMoviesApi()->getMovie($id);
        } catch (TmdbApiException $e){
            return $this->json([
                'error' => $e->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }

        if (empty($favoriteMovieRepo->findOneBy(['user' => $user, 'movie_id' => $id]))) {
            $favoriteMovie = new FavoriteMovie();
            $favoriteMovie->setMovieId($id);
            $favoriteMovie->setUser($user);
            $favoriteMovie->setDateAdded($current_date);
            $favoriteMovie->setDateModified($current_date);

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json([
                'message' => "You added the movie to your list"
            ], Response::HTTP_OK);
        }

        return $this->json([
            'message' => "You cannot add a movie twice at favorite"
        ], Response::HTTP_FORBIDDEN);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * DELETE Route annotation.
     * @Delete("/api/v1/favorites/{id<[0-9]+>}")
     */
    public function deleteFavoriteMovie($id, Request $request)
    {
        $authService = new AuthApiTokenService($this->getDoctrine()->getManager());
        $entityManager = $this->getDoctrine()->getManager();
        $user = $authService->checkAuthToken($request);
        $favoriteMovieRepo = $entityManager->getRepository(FavoriteMovie::class);

        if (!$user) {
            return $this->json([
                'error' => "You are not authorized!"
            ], Response::HTTP_UNAUTHORIZED);
        }

        $favoriteMovie = $favoriteMovieRepo->findOneBy(['user' => $user, 'movie_id' => $id]);

        if (empty($favoriteMovie)) {
            return $this->json([
                'error' => "You dont have this movie id at favorite"
            ], Response::HTTP_NOT_FOUND);
        }

        $entityManager->remove($favoriteMovie);
        $entityManager->flush();

        return $this->json([
            'message' => "You have deleted the movie from your list"
        ], Response::HTTP_OK);
    }

    /**
     * @throws \Exception
     * GET Route annotation.
     * @Get("/api/v1/get-token")
     */
    public function getToken()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $token         = bin2hex(random_bytes(10));
        $current_date  = new \DateTime();

        $user = new User();
        $user->setApiToken($token);
        $user->setDateAdded($current_date);
        $user->setDateModified($current_date);

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json([
            'token' => $user->getApiToken()
        ], Response::HTTP_OK);
    }
}