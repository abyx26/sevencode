<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apiToken;

    /**
     * @ORM\Column(name="date_added", type="datetimetz", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $dateAdded;

    /**
     * @ORM\Column(name="date_modified", type="datetimetz",options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $dateModified;

    /**
     * @ORM\OneToMany(targetEntity="\App\Entity\FavoriteMovie", mappedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="user_id", nullable=false)
     */
    protected $favoriteMovies;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    /**
     * @param string $apiToken
     * @return User
     */
    public function setApiToken(string $apiToken): self
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * @return Mixed_|null
     */
    public function getDateAdded(): ?Mixed_
    {
        return $this->dateAdded;
    }

    /**
     * @param mixed $dateAdded
     * @return User
     */
    public function setDateAdded($dateAdded): self
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * @return Mixed_|null
     */
    public function getDateModified(): ?Mixed_
    {
        return $this->dateModified;
    }

    /**
     * @param mixed $dateModified
     * @return User
     */
    public function setDateModified($dateModified): self
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getFavoriteMovies()
    {
        return $this->favoriteMovies;
    }

    /**
     * @param FavoriteMovie $favoriteMovie
     */
    public function addFavoriteMovie($favoriteMovie): void
    {
        $this->favoriteMovies[] = $favoriteMovie;
    }

    /**
     * @param $favoriteMovie
     * @return User
     */
    public function removeFavoriteMovie($favoriteMovie): self
    {
        $this->favoriteMovies->removeElement($favoriteMovie);

        return $this;
    }
}
