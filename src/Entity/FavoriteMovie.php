<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FavoriteMovieRepository")
 */
class FavoriteMovie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $movie_id;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\User", inversedBy="favoriteMovies")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(name="date_added", type="datetimetz", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $dateAdded;

    /**
     * @ORM\Column(name="date_modified", type="datetimetz",options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $dateModified;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getMovieId(): ?int
    {
        return $this->movie_id;
    }

    /**
     * @param int $movie_id
     * @return FavoriteMovie
     */
    public function setMovieId(int $movie_id): self
    {
        $this->movie_id = $movie_id;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return FavoriteMovie
     */
    public function setUser($user): self
    {
        $this->user = $user;
        $user->addFavoriteMovie($this);

        return $this;
    }

    /**
     * @return Mixed_|null
     */
    public function getDateAdded(): ?Mixed_
    {
        return $this->dateAdded;
    }

    /**
     * @param mixed $dateAdded
     * @return FavoriteMovie
     */
    public function setDateAdded($dateAdded): self
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * @return Mixed_|null
     */
    public function getDateModified(): ?Mixed_
    {
        return $this->dateModified;
    }

    /**
     * @param mixed $dateModified
     * @return FavoriteMovie
     */
    public function setDateModified($dateModified): self
    {
        $this->dateModified = $dateModified;

        return $this;
    }
}
