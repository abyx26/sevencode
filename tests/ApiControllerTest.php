<?php
/**
 * Created by PhpStorm.
 * User: ileda
 * Date: 2019-03-10
 * Time: 1:40 PM
 */

namespace App\Tests;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ApiControllerTest extends WebTestCase
{
    private $user = '48db13c7ad4211954b3d';
    private $link = 'http://dev.seven.code/';

    public function testGetToken()
    {
        $client = new Client();
        $response = $client->get($this->getLink() . 'api/v1/get-token');

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetMovieById()
    {
        $client = new Client();
        $response = $client->get(
            $this->getLink() . 'api/v1/movies?' . http_build_query(['query' => '500', 'api_token' => $this->getUser()]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetMovieByTitle()
    {
        $client = new Client();
        $response = $client->get(
            $this->getLink() . 'api/v1/movies?' . http_build_query(['query' => 'avengers', 'api_token' => $this->getUser()]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testIfAuthApiTokenIsWorking()
    {
        $client = static::createClient();
        $client->request(
            "GET", $this->getLink() . 'api/v1/movies?' . http_build_query(['query' => '500']));

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public function testAddFavoriteMovie()
    {
        $client = new Client();
        $response = $client->put(
            $this->getLink() . 'api/v1/favorites/500?' . http_build_query(['api_token' => $this->getUser()]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetFavoriteMovies()
    {
        $client = new Client();
        $response = $client->get(
            $this->getLink() . 'api/v1/favorites?' . http_build_query(['api_token' => $this->getUser()]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testDeleteFavoriteMovie()
    {
        $client = new Client();
        $response = $client->delete(
            $this->getLink() . 'api/v1/favorites/500?' . http_build_query(['api_token' => $this->getUser()]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }
}